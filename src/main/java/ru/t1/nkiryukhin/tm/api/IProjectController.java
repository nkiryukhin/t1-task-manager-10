package ru.t1.nkiryukhin.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();
}
