package ru.t1.nkiryukhin.tm.api;

public interface ITaskController {
    void createTask();

    void showTasks();

    void clearTasks();
}
