package ru.t1.nkiryukhin.tm.api;

import ru.t1.nkiryukhin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    void clear();

}
