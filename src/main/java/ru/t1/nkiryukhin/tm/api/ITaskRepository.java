package ru.t1.nkiryukhin.tm.api;

import ru.t1.nkiryukhin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    List<Task> findAll();

    void clear();

}
